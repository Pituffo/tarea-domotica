~Integrantes:-Felipe Salas Ortiz / 202030544-7 / felipe.salaso@sansano.usm.cl
            -Pedro Cisternas Arce / 202056597-k / pedro.cisternasa@usm.cl
            -Rodrigo Lobos / 201904649-7 / rodrigo.lobosc@usm.cl

~Asignatura:ELO-329 2022-1

~Profesor:-Pascal Ernesto Siguel Olivares

~Ayudante:-Matias Soto

~Fecha de Entrega:29/04/2022

~Paralelo:201

~Carrera:Ingenieria Civil Telematica

~Descripcion:
            La creacion de este proyecto se basa en una situacion real y que tiene como objetivo la simulacion de la tecnologia "Domotica" empleada en objetos cotidianos tales como lamparas y cortinas, las cuales mediantes la utilizacion de un control remoto llevarian a cabo diferentes tareas, en el caso se la lampara, este puede varias sus tonalidades mediante el patron RGB, ademas de encendido o apagado de este. En el caso de las cortinas, el control remoto presentaria las funciones de subir o bajar la tela, es decir extender y recojer esta segun requerimientos del usuario. El objetivo principal es la automatizacion del hogar (prioritariamente) o cualquier otro ambiente donde se implementen dichos objetos.

            En una primera etapa se incorporo/vinculo el metodo "ON/OFF" de la lampara, el cual emite diferentes senales y los registra en una "nube", quien esta encargada de conectarse con el control remoto para llevar a cabo la accion solicitada.

            A continuacion en una segunda etapa, ocurre una situacion similar a la de la primera etapa, la diferencia es que ahora trabajaremos con cortinas y en consecuencia presentaremos distintos parametros y senales, es decir que tendremos senales las cuales se enviaran a una "nube", y estas seran enviadas a un control remoto el cual operara la extension o recogimiento de la tela.

            Posteriormente tendremos una tercera etapa
            
            Para el diseño de este código de ayuda, se modeló "el mundo real" y nos imaginamos situaciones como:
Para cumplir con las tareas especificadas en el archivo de entrada, una persona (operador) debe revisar las líneas de éste. Una vez que se llegue al tiempo señalado, identifica el tipo de dispositivo domótico, toma aquel asociado al canal y efectúa la operación solicitada. Así opera la parte dinámica de la "simulación".
En el diseño estático de la etapa 1, se pensó que cada lámpara debe "registrarse" en la nube para estar visible a los controles.
Los controles deben tener acceso a la nube, así cuando el operador acciona un botón del control, el control pide a la nube ordenar que las lámaras asociadas a un canal específico cambien según el boton presionado.

~Requisitos de compilacion:
            -Compilable en Windows y Linux

~Requisitos para la compilacion:
            -tener instalado c++ y su compilados g++

~Como Compilar:
            -Descargar los archivos de la carpeta src.
            -Ir a la terminal.
            -Acceder al directorio donde se descargo los archivos.
            -Una vez dentro introducir como comando "g++ -o Proyecto funcioneprls.cpp proyectoprls.cpp -Wall".
            -A continuacion introducir "./Proyecto".

~Referencias bibliograficas:
